module.exports = function(locations){
        
    var gulp = require('gulp');
    var runSequence = require('run-sequence');

    var fontSources = [
        './node_modules/font-awesome/fonts/fontawesome-webfont.{eot,svg,ttf,woff,woff2}',
        './node_modules/font-awesome/fonts/FontAwesome.otf',
        './node_modules/slick-carousel/slick/fonts/slick.{eot,svg,ttf,woff,woff2}',
        './node_modules/video.js/dist/font/VideoJs.{eot,svg,ttf,woff}',
        './assets/fonts/673377/*.{eot,svg,ttf,woff}',
        './assets/fonts/692692/*.{eot,svg,ttf,woff}',
        './assets/fonts/helveticabold/*.{eot,svg,ttf,woff,woff2}',
        './assets/fonts/helvetica-lite/*.{eot,svg,ttf,woff,woff2}',
        './assets/fonts/helvetica-regular/*.{eot,svg,ttf,woff,woff2}'
    ];

    var imageSources = [
        './assets/img/*.*'
    ];

    var swfSources = [
        './assets/src/swf/*.*'
    ]

    gulp.task('assets', function() {
        runSequence(
            'copyFonts',
            'copySwf',
            'copyImages'
        )
    });

    gulp.task('copyFonts', function(){
        return gulp.src(fontSources)
        .pipe(gulp.dest(locations.dest + '/fonts'));
    })

    gulp.task('copySwf', function(){
        return gulp.src(swfSources)
        .pipe(gulp.dest(locations.dest + '/swf'));
    })

    gulp.task('copyImages', function(){        
        return gulp.src(imageSources)
        .pipe(gulp.dest(locations.dest + '/img'));
    })

}