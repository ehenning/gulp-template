module.exports = function(locations){
    var gulp = require('gulp');
    var del = require('del');

    gulp.task('clean', function(){

        //using force:true because the gulp file is not in the parent folder 
        return del(locations.dest, {force:true}).then(paths => {
            console.log('Files and folders that would be deleted:\n', paths.join('\n'));
        });
    });

}