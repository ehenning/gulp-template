module.exports = function(locations){

    var gulp = require('gulp');
    var runSequence = require('run-sequence');
    var concat = require('gulp-concat');
    var uglify = require('gulp-uglify');

    var vendorSources = [
        './node_modules/arrive/minified/arrive.min.js',
        './node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        './node_modules/chosen-js/chosen.jquery.js',
        './node_modules/clamp-js/clamp.js',
        './node_modules/fancybox/dist/js/jquery.fancybox.pack.js',
        './node_modules/jquery-match-height/dist/jquery.matchHeight-min.js',
        './node_modules/maplace-js/dist/maplace.min.js',
        './node_modules/slick-carousel/slick/slick.min.js',
        './node_modules/video.js/dist/video.min.js',
        './node_modules/videojs-vimeo/src/Vimeo.js',
        './node_modules/videojs-youtube/dist/Youtube.min.js',
    ];

    gulp.task('scripts', function(){
        runSequence(
            'scriptsVendor'
        )
    });

    gulp.task('scriptsVendor', function(){
        return gulp.src(vendorSources)
            .pipe(concat('vendor.js'))
            .pipe(gulp.dest(locations.dest + '/js'));
    })


}