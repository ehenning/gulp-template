module.exports = function(locations){

    var gulp = require('gulp');
    var sass = require('gulp-sass');
    var sourcemaps = require('gulp-sourcemaps');
    var autoprefixer =  require('gulp-autoprefixer');
    var concat = require('gulp-concat');
    var moduleImporter = require('sass-module-importer');
    var path = require('path');

    var sassOptions = {
        errLogToConsole: true,
        outputStyle: 'compressed',
        importer: moduleImporter({
            basedir: path.join(__dirname, 'node_modules') }
        )
    };

    var autoprefixerOptions = {
        //browsers: ['last 2 versions', '> 5%']
        browsers: [
            'Chrome >= 35',
            'Edge >= 12',
            'Explorer >= 9',
            'iOS >= 8',
            'Safari >= 8',
            'Android 2.3',
            'Android >= 4',
            'Opera >= 12'
        ]
    };

    var sassSources = [
        './assets/src/**/*.scss',
        './mvc/views/**/*.scss'
    ];

    gulp.task('sass', function(){
        return sassFiles = gulp
            .src(sassSources)
            .pipe(sourcemaps.init())
            .pipe(sass(sassOptions).on('error', sass.logError))
            .pipe(autoprefixer(autoprefixerOptions))  
            .pipe(concat('index.css'))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(locations.dest + '/css'));
    });

}