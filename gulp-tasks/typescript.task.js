module.exports = function(locations){

    var gulp = require('gulp');
    var runSequence = require('run-sequence');
    var uglify = require('gulp-uglify');
    var concat = require('gulp-concat');
    var typescript = require('gulp-typescript');
    var sourcemaps = require('gulp-sourcemaps');

    var tsProject = typescript.createProject('./tsconfig.json');

    var typescriptSources = [
        "./mvc/scripts/**/*.ts",
        "./mvc/**/*.ts"
    ];

    gulp.task('typescript', function(){
       var tsResult = gulp.src(typescriptSources)
            .pipe(sourcemaps.init())
            .pipe(tsProject());

        return tsResult.js 
            .pipe(concat('scripts.js'))
            //.pipe(uglify())
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(locations.dest + '/js'))
    });

}