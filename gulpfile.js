/// <binding AfterBuild='default' Clean='clean' />

var gulp = require('gulp');
var runSequence = require('run-sequence');

var locations = {
    dest: './assets/dist',
}

require('./gulp-tasks/clean.task')(locations);
require('./gulp-tasks/typescript.task')(locations);
require('./gulp-tasks/scripts.task')(locations);
require('./gulp-tasks/assets.task')(locations);
require('./gulp-tasks/styles.task')(locations);

gulp.task('default', function(){
    runSequence(
        'clean',
        'typescript',
        'scripts',
        'assets',
        'sass'
    )
})
